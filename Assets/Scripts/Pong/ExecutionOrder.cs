﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecutionOrder : MonoBehaviour
{
    public int numeroDeTest = 0;
    public GameObject borde;
   
    private void Update()
    {
        Debug.Log("Estoy en Update" + numeroDeTest);
        numeroDeTest++;
        borde.transform.position += new Vector3(1,1,1);
    }

    private void LateUpdate()
    {
        Debug.LogWarning("Estoy en LateUpdate");
    }
}


