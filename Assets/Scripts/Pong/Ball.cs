﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject GameMaster;
    
    private Vector2 ballPosition;
    public Vector2 ballVelocity;
    private Vector2 startPosition;

    void Awake()
    {
        ballPosition = transform.position;
        startPosition = transform.position;
}

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);

        if (GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().gameRestarting == true)
        {
            transform.position = startPosition;

            if (ballVelocity.y < 0)
            {
                ballVelocity.y *= -1;
            }
            
        }
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "MuroLateral")
        {
            ballVelocity.x *= -1;
        }
        else if (other.gameObject.tag == "MuroVertical")
        {
            ballVelocity.y *= -1;
        }
        /*else if (other.gameObject.tag == "EnemyGoal")
        {
            Debug.Log("Hemos marcado un gol");
            transform.position = new Vector3(0, 0, 0);
            ballVelocity.x = -3;
            ballVelocity.y = -2;
        }
        else if (other.gameObject.tag == "PlayerGoal")
        {
            Debug.Log("Eres un paquete");
            transform.position = new Vector3(0, 0, 0);
            ballVelocity.x = 3;
            ballVelocity.y = 2;
        }*/
        else if (other.gameObject.tag == "Pala")
        {
            
            ballVelocity.y *= -1;
        }
        else if (other.gameObject.tag == "Bloque")
        {
            /*contactPoint = other..contacts[0].point;
            currentNormal = other.;

            bool right = contactPoint.x > center.x;
            bool top = contactPoint.y > center.y;*/
            ballVelocity.y *= -1;
            GameMaster.GetComponent<GameMaster>().score += 10;
            GameMaster.GetComponent<GameMaster>().ScoreText.GetComponent<UnityEngine.UI.Text>().text = GameMaster.GetComponent<GameMaster>().score.ToString();
        }

        else if (other.gameObject.tag == "DeadZone")
        {
            GameMaster.GetComponent<GameMaster>().lives--;
            GameMaster.GetComponent<GameMaster>().LivesText.GetComponent<UnityEngine.UI.Text>().text = GameMaster.GetComponent<GameMaster>().lives.ToString();
            transform.position = startPosition;
            ballVelocity.y *= -1;
        }
    }


}