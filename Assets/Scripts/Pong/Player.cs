﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadX;
    private float posX;
    private float direction;
    private Vector3 newPosition = new Vector3(0, 0, 0);
    public float maxPosition;
    private Vector3 startPosition;

    // Update is called once per frame

    private void Start()
    {
        startPosition = transform.position;
    }
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        /*Debug.Log(transform.position.y);
        Debug.Log(transform.rotation.y);*/

        posX = transform.position.x + direction * velocidadX * Time.deltaTime;

        newPosition.y = transform.position.y;
        newPosition.x = posX;
        newPosition.z = transform.position.z;
        

        if (newPosition.x < maxPosition -1.1 && newPosition.x > -1.2 || newPosition.x > -maxPosition -1.2 && newPosition.x < -1.2)
        {
            transform.position = newPosition;
            
        }

        if (GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().gameRestarting == true)
        {
            transform.position = startPosition;
        }
    }

    
}