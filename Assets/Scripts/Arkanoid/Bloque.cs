﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloque : MonoBehaviour
{
    // Start is called before the first frame update


    Vector3 restartPosition;
    Vector3 destroyedPosition;
    void Start()
    {
        restartPosition.y = transform.position.y;
        restartPosition.x = transform.position.x;
        restartPosition.z = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().gameRestarting == true) 
        {
            transform.position = restartPosition;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ball") 
        {
            destroyedPosition.x = transform.position.x + 200;
            destroyedPosition.y = transform.position.y;
            destroyedPosition.z = transform.position.z;

            transform.position = destroyedPosition;
        }
    }
}
