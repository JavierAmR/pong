﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public GameObject ScoreText;
    public GameObject PauseMenu;
    public GameObject LivesText;
    public GameObject RestartMenu;

    public int score = 0;
    public int lives = 3;
    bool restartActive = false;
    public bool gameRestarting = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //Pausa
        if (Input.GetKeyDown("p"))
        {
            if (Time.timeScale != 0) 
            {
                Time.timeScale = 0;
                PauseMenu.SetActive(true);
            }
            else 
            {
                Time.timeScale = 1;
                PauseMenu.SetActive(false);
            }
            
        }
        if (gameRestarting == true) 
        {
            gameRestarting = false;
            Time.timeScale = 1;
        }

        if (restartActive == true && Input.GetKeyDown("r"))
        {
            Time.timeScale = 1;
            RestartMenu.SetActive(false);
            restartActive = false;
            gameRestarting = true;
            lives = 3;
            score = 0;
            ScoreText.GetComponent<UnityEngine.UI.Text>().text = score.ToString();
            LivesText.GetComponent<UnityEngine.UI.Text>().text = lives.ToString();

        }

        if (lives == 0 && restartActive == false && gameRestarting == false || score == 780 && restartActive == false && gameRestarting == false) 
        {
            Time.timeScale = 0;
            RestartMenu.SetActive(true);
            restartActive = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            Application.Quit();
        }
    }
}
